var express = require('express');
var session = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var busboy = require('express-busboy');
//var cookieSession = require('cookie-session')
//gzip to shrink http payload
var compression = require('compression');
var cookieSession = require('cookie-session');

var app = express();

// GZIP all assets
app.use(compression());

// view engine setup, pug
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, '/app/server/views'));

busboy.extend(app, {
  upload: true
  //path: 'https://promoxdata.blob.core.windows.net/images/'
});

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(cookieParser());

//TODO: THIS is the first one using MemoryStore
/*app.use(session({
    secret: 'faeb4453e5d14fe6f6d04637f78077c76c73d1b4',
    proxy: true,
    resave: true,
    saveUninitialized: true
  })
);*/

app.use(cookieSession({
  name: 'session',
  secret: 'faeb4453e5d14fe6f6d04637f78077c76c73d1b4',

  // Cookie Options
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}));


app.use(express.static(path.join(__dirname, '/app/public')));

require('./app/server/routes/routes')(app);

module.exports = app;
