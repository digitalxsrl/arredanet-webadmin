/**
 * Crafted with ♥ by Alex on 22/11/2016, updated daily
 */

var req_core = require('../helpers/service/reqcore');

var loginController = require('../controllers/LoginController');
var homeController = require('../controllers/HomeController');
var homePanelsController = require('../controllers/HomePanelsController');
var productController = require('../controllers/ProductController');
var promoController = require('../controllers/PromoController');
var newsController = require('../controllers/NewsController');
var qrCodeController = require('../controllers/QrCodeController');
var urlController = require('../controllers/UrlController');
var mediaController = require('../controllers/MediaController');
var galleryController = require('../controllers/GalleryController');
var attachedFileController = require('../controllers/AttachedFilesController');
var uploadController = require('../controllers/UploadController');

//Defining the routes
module.exports = function (app) {

  /* GET login page. */
  app.get('/', loginController.rootGetForLogin);
  /* POST login page for auth */
  app.post('/', loginController.rootPostForLogin);
  /* GET logout page wich actually performs a client side logout */
  app.get('/admin/logout', loginController.logout);


  /* GET home page. */
  app.get('/admin/home', homeController.getHome);

  /* GET products page*/
  app.get('/admin/products', productController.getProductPage);


  /*
   * Home Top Panels counter requests
   * */
  app.get('/admin/stats/totalQr', homePanelsController.getTotalQr);
  app.get('/admin/stats/totalPromo', homePanelsController.getTotalActivePromo);
  app.get('/admin/stats/totalNews', homePanelsController.getTotalNews);
  app.get('/admin/stats/totalAttach', homePanelsController.getTotalAttachedFiles);

  /**
  * Product objects page handling
  * */
  //Gallery
  app.get('/admin/product-gallery-list/:idProduct?', galleryController.getGalleryList);
  app.get('/admin/product-gallery-detail/:galleryId?', galleryController.getGalleryDetail);
  app.delete('/admin/product-gallery-detail/:galleryId', galleryController.deleteGallery);
  app.put('/admin/product-gallery-detail/:galleryId', galleryController.updateGallery);
  app.post('/admin/product-gallery-detail', galleryController.postGallery);
  app.post('/admin/product-gallery-detail/file', galleryController.postFile); //Posting File
  app.post('/admin/product-gallery-detail/deletefile', galleryController.deleteFile);

  //Getting product attach list & detail
  app.get('/admin/product-attached-list/:idProduct?', attachedFileController.getProductAttachedList);
  app.get('/admin/product-attached-detail/:attachmentId?', attachedFileController.getProductAttachedDetail);
  app.delete('/admin/product-attached-detail/:id', attachedFileController.deleteProductAttached);
  app.put('/admin/product-attached-detail/:id', attachedFileController.updateProductAttached);
  app.post('/admin/product-attached-detail', attachedFileController.postAttachment);
  app.post('/admin/product-attached-detail/file', attachedFileController.postFile); //Posting File

  //Getting media list & detail
  app.get('/admin/product-media-list/:idProduct?', mediaController.getProductMediaList);
  app.get('/admin/product-media-detail/:mediaId?', mediaController.getProductMediaDetail);
  app.put('/admin/product-media-detail/:mediaId', mediaController.putMedia); //Url update
  app.post('/admin/product-media-detail', mediaController.postMedia); //Insert new url
  app.delete('/admin/product-media-detail/:mediaId', mediaController.deleteMedia); //Remove a url


  /**
   * Promotions
   * */
  app.get('/admin/promos', promoController.getPromoList);
  app.get('/admin/promo-detail/:promoId?', promoController.getPromoDetail);
  app.put('/admin/promo-detail/:promoId?', promoController.putPromo); //Promo update
  app.post('/admin/promo-detail', promoController.postPromo); //Insert new promo
  app.delete('/admin/promo-detail/:promoId?', promoController.deletePromo); // Remove a promo
  app.post('/admin/promo-detail/photo', promoController.postNewsMainImage); //Posting Image
  app.post('/admin/promo-detail/file', promoController.postFile); //Posting File

  /**
   * News
  * */
  app.get('/admin/newses', newsController.getNewsList); //Get list of news
  app.get('/admin/news-detail/:newsId?', newsController.getNewsDetail); //Detail of news
  app.put('/admin/news-detail/:newsId?', newsController.putNews); //News update
  app.post('/admin/news-detail', newsController.postNews); //Insert new news
  app.delete('/admin/news-detail/:newsId?', newsController.deleteNews); // Remove a news
  app.post('/admin/news-detail/photo', newsController.postNewsMainImage); //Posting Image
  app.post('/admin/news-detail/file', newsController.postFile); //Posting File

  /**
   * Urls
   * */
  app.get('/admin/urls', urlController.getUrlList);
  app.get('/admin/url-detail/:idUrl?', urlController.getUrlDetail);
  app.put('/admin/url-detail/:idUrl?', urlController.putUrl); //Url update
  app.post('/admin/url-detail', urlController.postUrl); //Insert new url
  app.delete('/admin/url-detail/:idUrl?', urlController.deleteUrl); //Remove a url

  /**
   * QRCode
   * */
  app.get('/admin/qrcodes', qrCodeController.getQrCodeList); //Get list of qrcodes
  app.get('/admin/qrcode-detail/:qrCodeId?', qrCodeController.getQrCodeDetail); //Get qrcode details
  app.get('/admin/qrcode-resources', qrCodeController.getQrCodeResources); //Get qrcode resource list by type
  app.post('/admin/qrcode-detail', qrCodeController.postQrCode); //Insert new qrcode
  app.put('/admin/qrcode-detail/:qrCodeId?', qrCodeController.putQrCodeDetail); //qrcode update

  /**
   * Utility to remove session image or file already uploaded or presetted via get obj
   * */

  app.post('/admin/content/fileRemove', uploadController.removeFile);
  app.post('/admin/content/imageRemove', uploadController.removeImage);

  //Utility exposed to encode a password
  app.get('/encoder/:pwd', function(req, res) {
    res.send('['+ req_core.hashPassword(req.params.pwd) + ']');
  });

  // Handle 404
  app.use(function(req, res) {
     res.render('404', { title: 'Pagina non trovata'});
  });

  // Handle 500
  app.use(function(error, req, res, next) {
    res.render('500', { title: 'Si è verificato un errore'});
  });
};