/**
 * Crafted with ♥ by Alex on 28/12/2016.
 */

"use strict";
var MODULE_NAME = 'Gallery Controller';

//Added utility class helper
var utility = require('../helpers/util');
var req_core = require('../helpers/service/reqcore');
var _ = require('lodash');

var uploadController = require('./UploadController');
var qrCodeController = require('./QrCodeController');

/**
 * gets the home page of the CMS
 * @param req
 * @param res
 */
function getGalleryList(req, res) {
  //Eventually pass data to the page render obj
  var pageRender = {};

  //Building page components, asking them to a config
  pageRender.menu = utility.getMenuForPage(1);
  pageRender.pageHeader = utility.getPageHeader(18);

  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {

    var idProduct = req.params.idProduct;

    req_core.getAllProductGallery(token, idProduct, function (e,o) {
      if(!e && o) {
        pageRender.productGalleryList = JSON.stringify(o);
        pageRender.productId = idProduct;
        pageRender.userMail = req.session.userMail;

        res.render('admin/product-gallery-list', pageRender);
      }
    });
  }

}

function getGalleryDetail(req, res) {
  //Eventually pass data to the page render obj
  var pageRender = {};
  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {
    //Building page components, asking them to a config
    pageRender.menu = utility.getMenuForPage(1);// Passing 0 because 0 is home page

    var galleryId = req.params.galleryId;

    if(galleryId) {
      req_core.getGalleryById(token, galleryId, function (e, o) {
        if (!e && o) {
          pageRender.pageHeader = utility.getPageHeader(20);
          //adding to session current filename if exist
          //TODO GALLERY FILES
          pageRender.galleryObj = o[0];

          /*pageRender.galleryObj.Nomi_File = _.forEach(pageRender.galleryObj.Immagini, function (item){
            var splitted = _.split(item,'/');
            item = splitted[splitted.length -1];
          });*/

          pageRender.galleryObj.Nomi_File = _.map(pageRender.galleryObj.Immagini,function(item){

            var splitted = _.split(item,'/');

            return {'id': splitted[splitted.length -1], 'imageName': splitted[splitted.length -1]}

          });

          pageRender.galleryObj.initialPreviewConfig = _.map(pageRender.galleryObj.Immagini, function (item){
            var splitted = _.split(item,'/');
            return {type: "image", width: "120px", url: "/admin/product-gallery-detail/deletefile", key: splitted[splitted.length -1]}
          });

          //Only if gallery has a QR code obj..
          if(o[0].QRCode) {
            pageRender.galleryObj.QRCode = qrCodeController.buildQrCodeObj(o[0].QRCode);
          }else {
            pageRender.galleryObj.QRCode = {};
          }

          pageRender.galleryObj.Id_prodotto = o[0].Id_prodotto;
          pageRender.gallery = JSON.stringify(pageRender.galleryObj);
          res.render('admin/product-gallery-detail', pageRender);
        }
      });
    }else {
      pageRender.pageHeader = utility.getPageHeader(19);
      pageRender.galleryObj = {};
      pageRender.galleryObj.QRCode = {};
      pageRender.galleryObj.Id_prodotto = req.query.productId;
      pageRender.gallery = JSON.stringify(pageRender.galleryObj);
      res.render('admin/product-gallery-detail', pageRender);
    }
  }
}

function postGallery(req, res) {
  var token = req.session.token;
  if(token == null) {
    res.redirect('/');
  }else {

    var obj = _buildGallery(req);

    if(!obj.Immagini ||(obj.Immagini && obj.Immagini.length == 0)){
      res.status(400).send({message:'Inserisci le immagini della galleria.'});
      return;
    }
    //var objJson = JSON.stringify(obj); Commented, useless, stringify is down in req_core method!
    var qrCode = req.body.cbQrCode;
    var qrCodeTitle = req.body.TitoloQrCode;

    req_core.insertGallery(token, obj, function (e, o, statusCode) {
      if (!e && o && (statusCode == 200 || statusCode == 201)) {
        //Eventually clear some session variables eg: Image or attached file when inserted
        if(qrCode && qrCode === 'on'){
          var qrObj  = {};
          qrObj.Nome = qrCodeTitle ? qrCodeTitle : utility.generateRandomName();
          qrObj.Tipo_risorsa = 'Tab_Gallerie';
          qrObj.Id_risorsa = parseInt(o.Id);
          req_core.assignToFreeQr(token,qrObj,function (e,o){
            if(!e && o ){
              res.status(statusCode).send('{"status":"ok"}');
            }else {
              res.status(statusCode).send(e);
            }
          })
        }else {
          //Insert w/o a QR
          res.status(statusCode).send('{"status":"ok"}');
        }
      } else {
        res.status(statusCode).send(e);
      }
    });
  }
}

function updateGallery(req, res) {

  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {

    var galleryId = req.params.galleryId;
    var gallery = _buildGallery(req);

    //CHECK IF QRCODE AND REMOVE OR SET
    var qrCode = req.body.cbQrCode;
    var qrCodeTitle = req.body.TitoloQrCode;
    var Id_qrcode = req.body.Id_qrcode;


    if(!qrCode && Id_qrcode) {
      //Removing an assigned qrcode from a resource
      gallery.Id_qrcode_old = parseInt(Id_qrcode);
      gallery.Id_qrcode = -1;
    }

    if(!gallery.Immagini ||(gallery.Immagini && gallery.Immagini.length == 0)){
      res.status(400).send({message:'Inserisci le immagini della galleria.'});
      return;
    }

    req_core.updateGallery(token, gallery, galleryId, function (e,o) {
      if(!e && o) {
        if(qrCode && qrCode === 'on' && !Id_qrcode){
          var qrObj  = {};
          qrObj.Nome = qrCodeTitle ? qrCodeTitle : utility.generateRandomName();
          qrObj.Tipo_risorsa = 'Tab_Gallerie';
          qrObj.Id_risorsa = parseInt(o.Id);
          req_core.assignToFreeQr(token,qrObj,function (e,o){
            if(!e && o ){
              res.status(200).send('{"status":"ok"}');
            }else {
              res.status(500).send(e);
            }
          })
        }else {
          res.status(200).send('{"status":"ok"}');
        }
      }else {
        res.status(500).send(e);
      }
    });
  }
}

function deleteGallery(req, res) {

  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {
    var galleryId = req.params.galleryId;
    req_core.deleteGallery(token, galleryId, function (e,o) {
      if(!e && o) {
        res.status(200).send('{"status":"ok"}');
      }else {
        res.status(500).send(e);
      }
    });
  }
}

function _buildGallery (req) {
  var obj = {};

  obj.Nome = req.body.Nome;

  //req.body.galleryImg contains uploaded images client side
  obj.Immagini = JSON.parse(req.body.galleryImg);

  obj.Id_prodotto = req.query.productId;

  return obj;

}

function postFile (req, res) {
  uploadController.postFile(req,res,uploadController.formData().resource.GALLERY, uploadController.formData().type.image);
}

function deleteFile(req, res) {
  res.status(200).send({});
}

function clone(obj) {
  if (null == obj || "object" != typeof obj) return obj;
  var copy = obj.constructor();
  for (var attr in obj) {
    if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
  }
  return copy;
}


exports.getGalleryList = getGalleryList;
exports.getGalleryDetail = getGalleryDetail;
exports.postGallery = postGallery;
exports.updateGallery = updateGallery;
exports.deleteGallery = deleteGallery;
exports.postFile = postFile;
exports.deleteFile = deleteFile;