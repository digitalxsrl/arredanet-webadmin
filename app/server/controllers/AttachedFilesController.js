/**
 * Crafted with ♥ by Alex on 28/12/2016.
 */

"use strict";
var MODULE_NAME = 'Attached File Contoller';

//Added utility class helper
var utility = require('../helpers/util');
var req_core = require('../helpers/service/reqcore');

var uploadController = require('./UploadController');
var qrCodeController = require('./QrCodeController');

const attachmentTypes = ['CATALOGO_GENERALE', 'CATALOGO_VERTICALE','SCHEDA_FUNZIONALE', 'SCHEDA_TECNICA'];

function getProductAttachedList(req, res) {
  //Eventually pass data to the page render obj
  var pageRender = {};

  //Building page components, asking them to a config
  pageRender.menu = utility.getMenuForPage(1);
  pageRender.pageHeader = utility.getPageHeader(15);


  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {

    var idProduct = req.params.idProduct;

    req_core.getAllProductsAttached(token, idProduct, function (e,o) {
      if(!e && o) {
        pageRender.productAttachedList = JSON.stringify(o);
        pageRender.productId = idProduct;
        pageRender.userMail = req.session.userMail;

        res.render('admin/product-attached-list', pageRender);
      }
    });
  }
}

function getProductAttachedDetail(req, res) {
  //Eventually pass data to the page render obj
  var pageRender = {};
  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {
    req.session.uploadedFile = null; //CLEARING ALREADY UPLOADED AND SAVED ATTACHMENT FILE!

    //Building page components, asking them to a config
    pageRender.menu = utility.getMenuForPage(1);// Passing 0 because 0 is home page

    var attachmentId = req.params.attachmentId;

    if(attachmentId) {
      pageRender.pageHeader = utility.getPageHeader(17);
      req_core.getAttachmentById(token, attachmentId, function (e, o) {
        if (!e && o) {
          //adding to session current filename if exist
          if(o.Nome_allegato) {
            req.session.uploadedFile  = o.Nome_allegato;
          }

          pageRender.attachmentObj = o;
          pageRender.attachmentObj.QRCode = qrCodeController.buildQrCodeObj(o.QRCode);
          pageRender.attachmentTypes = attachmentTypes;

          pageRender.attachmentObj.Id_prodotto = o.Id_prodotto;

          //Setting url for file preview
          var ep = process.env['LCM_CONTENT_HOST_' + process.env.BUILD_TYPE];
          pageRender.attachmentObj.filePreview = ep + '/file/' + pageRender.attachmentObj.Nome_allegato;

          pageRender.attachment = JSON.stringify(pageRender.attachmentObj);
          res.render('admin/product-attached-detail', pageRender);
        }
      });
    }else {
      pageRender.pageHeader = utility.getPageHeader(16);
      pageRender.attachmentObj = {};
      pageRender.attachmentObj.QRCode = {};
      pageRender.attachmentObj.Id_prodotto = req.query.productId;
      pageRender.attachmentTypes = attachmentTypes;
      pageRender.attachment = JSON.stringify(pageRender.attachmentObj);
      res.render('admin/product-attached-detail', pageRender);
    }
  }
}

//Post a news
function postAttachment(req, res) {
  var token = req.session.token;
  if(token == null) {
    res.redirect('/');
  }else {
    var obj = _buildAttachment(req);
    if(!obj.Nome_allegato){
      res.status(400).send({message:'Allegato obbligatorio.'});
      return;
    }
    //var objJson = JSON.stringify(obj); Commented, useless, stringify is down in req_core method!
    var qrCode = req.body.cbQrCode;
    var qrCodeTitle = req.body.TitoloQrCode;

    req_core.insertAttachment(token, obj, function (e, o, statusCode) {
      if (!e && o && (statusCode == 200 || statusCode == 201)) {
        req.session.uploadedFile = null; //CLEARING ALREADY UPLOADED AND SAVED ATTACHMENT FILE!
        //Eventually clear some session variables eg: Image or attached file when inserted
        if(qrCode && qrCode === 'on'){
          var qrObj  = {};
          qrObj.Nome = qrCodeTitle ? qrCodeTitle : utility.generateRandomName();
          qrObj.Tipo_risorsa = 'Tab_Allegati';
          qrObj.Id_risorsa = o[0].Id;
          req_core.assignToFreeQr(token,qrObj,function (e,o){
            if(!e && o ){
              res.status(statusCode).send('{"status":"ok"}');
            }else {
              res.status(statusCode).send(e);
            }
          })
        }else {
          //Insert w/o a QR
          res.status(statusCode).send('{"status":"ok"}');
        }
      } else {
        res.status(statusCode).send(e);
      }
    });
  }
}

function updateProductAttached(req, res) {

  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {

    var id = req.params.id;
    var attachment = _buildAttachment(req);

    //CHECK IF QRCODE AND REMOVE OR SET
    var qrCode = req.body.cbQrCode;
    var qrCodeTitle = req.body.TitoloQrCode;
    var Id_qrcode = req.body.Id_qrcode;


    if(!qrCode && Id_qrcode) {
      //Removing an assigned qrcode from a resource
      attachment.Id_qrcode_old = parseInt(Id_qrcode);
      attachment.Id_qrcode = -1;
    }

    req_core.updateAttachment(token, attachment, id, function (e,o) {
      if(!e && o) {
        req.session.uploadedFile = null; //CLEARING ALREADY UPLOADED AND SAVED ATTACHMENT FILE!
        if(qrCode && qrCode === 'on' && !Id_qrcode){
          var qrObj  = {};
          qrObj.Nome = qrCodeTitle ? qrCodeTitle : utility.generateRandomName();
          qrObj.Tipo_risorsa = 'Tab_Allegati';
          qrObj.Id_risorsa = o.Id;
          req_core.assignToFreeQr(token,qrObj,function (e,o){
            if(!e && o ){
              res.status(200).send('{"status":"ok"}');
            }else {
              res.status(500).send(e);
            }
          })
        }else {
          res.status(200).send('{"status":"ok"}');
        }
      }else {
        res.status(500).send(e);
      }
    });
  }
}

function deleteProductAttached(req, res) {

  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {
    var id = req.params.id;
    req_core.deleteAttachment(token, id, function (e,o) {
      if(!e && o) {
        res.status(200).send('{"status":"ok"}');
      }else {
        res.status(500).send(e);
      }
    });
  }
}

function _buildAttachment(req) {
  var obj = {};
  //Setting title
  obj.Nome = req.body.Titolo;

  //Tipologia allegato
  obj.Tipo_allegato = req.body.ddlTypes;

  //Setting pdf name
  obj.Nome_allegato = clone(req.session.uploadedFile);

  //Setting product father it
  obj.Id_prodotto = parseInt(req.query.productId);
  obj.Nuova_piattaforma = true;

  return obj;
}

function postFile (req, res) {
  uploadController.postFile(req,res,uploadController.formData().resource.ATTACHMENT, uploadController.formData().type.file);
}

function clone(obj) {
  if (null == obj || "object" != typeof obj) return obj;
  var copy = obj.constructor();
  for (var attr in obj) {
    if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
  }
  return copy;
}


exports.getProductAttachedList = getProductAttachedList;
exports.getProductAttachedDetail = getProductAttachedDetail;
exports.updateProductAttached = updateProductAttached;
exports.deleteProductAttached = deleteProductAttached;
exports.postAttachment = postAttachment;
exports.postFile = postFile;