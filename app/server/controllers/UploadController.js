/**
 * Crafted with ♥ by Alex on 28/12/2016.
 */

const UPLOAD_FAIL = -1;
const MAX_TRY_NUM  = 5;
const MODULE_NAME = "Upload Controller";

var utility = require('../helpers/util');
var requestModule = require('request');
var fileStream = require('fs');


function postFile (req, res, risorsa, tipo) {
  _sendFileToServer(0,req,res, risorsa, tipo);
}

function _sendFileToServer(tryNumber, req, res, risorsa, tipo) {

  // avoid continuous recursion
  tryNumber++;
  if(tryNumber > MAX_TRY_NUM){
    //FIXME: Send back error upload image!
    res.send(JSON.stringify({error:'Non è stato possibile caricare il file...'}));
    return;
  }

  // create the form for the image server request
  var formData = {};
  if(tipo == 'image'){
    formData = {
      content: fileStream.createReadStream(req.files.immagine.file)
    };
  }else if (tipo == 'file') {
    formData = {
      content: fileStream.createReadStream(req.files.allegato.file)
    };
  }


  // post the form
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE] + "/content";

  //Adding params to form data
  formData.risorsa = risorsa;
  formData.tipo = tipo;

  var header = {
    "Content-Type": "application/x-www-form-urlencoded",
    "token": req.session.token};

  requestModule.post({url:ep, headers: header, formData: formData}, function(err, httpResponse, body) {
    if (err || httpResponse.statusCode == 408) {
      req.session.image = UPLOAD_FAIL;
      _sendFileToServer(tryNumber, req ,res, risorsa, tipo);
      return console.error(MODULE_NAME + ': img upload failed with error:', err, " and statusCode: " + httpResponse.statusCode, "\n try num: " + tryNumber);
    }else if (body.length <= 0){
      console.log(MODULE_NAME + ": upload try failed " + body);
      _sendFileToServer(tryNumber, req, res, risorsa, tipo);
    }else {
      console.log(MODULE_NAME + ': upload successful!  Server responded with: ', body);

      var id = body;
      var jsonResponse;
      if(tipo == 'image' && risorsa == 'gallery'){
        /*//Actually pushing the new item in the array!
        if (!req.session.imageGallery) {
          req.session.imageGallery = [];
        }
        req.session.imageGallery.push(id);
        */
        //New logic:
        jsonResponse = JSON.stringify({
          append: true,
          imageId : id
        });
      }else if(tipo == 'image') {
        req.session.uploadedImage = id;
         jsonResponse = JSON.stringify({
          append: true,
          imageId : id
        });

      }else if (tipo == 'file') {
        req.session.uploadedFile = id;
        jsonResponse = JSON.stringify({
          append: true,
          fileId : id
        });

      }
      //sending response to plugin

      //sending response to UI!
      res.send(jsonResponse);

    }
  });
}

function formData () {
  return {
    type: {image: 'image', file: 'file'},
    resource: {OUTLET: 'outlet', NEWS: 'news-aderente', PROMO: 'promo', GALLERY: 'gallery', ATTACHMENT: 'allegato'}
  };
}


exports.postFile = postFile;
exports.formData = formData;

function removeFile (req, res) {
  req.session.uploadedFile = null;
  res.send({}); //Empty response as success
}

function removeImage (req, res) {
  req.session.uploadedImage = null;
  res.send({}); //Empty response as success
}

//
exports.removeFile = removeFile;
exports.removeImage = removeImage;