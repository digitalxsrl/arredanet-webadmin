/**
 * Crafted with ♥ by Alex on 29/11/2016.
 */

"use strict";
var MODULE_NAME = 'ProductController';

//Added utility class helper
var utility = require('../helpers/util');
var req_core = require('../helpers/service/reqcore');

function getProductPage(req, res) {
  //Eventually pass data to the page render obj
  var pageRender = {};

  //Building page components, asking them to a config
  pageRender.menu = utility.getMenuForPage(1);
  pageRender.pageHeader = utility.getPageHeader(1);

  //TODO: Get product list here... REMOVE THIS EXAMPLE!
  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {
    req_core.getAllProducts(token,function (e,o){
      if(!e && o){

        pageRender.productList = JSON.stringify(o);

        pageRender.userMail = req.session.userMail;

        res.render('admin/products', pageRender);
      }
    });
  }
}

//exports

exports.getProductPage = getProductPage;