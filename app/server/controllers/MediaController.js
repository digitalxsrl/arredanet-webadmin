/**
 * Crafted with ♥ by Alex on 28/12/2016.
 */

"use strict";
var MODULE_NAME = 'Media Contoller';

//Added utility class helper
var utility = require('../helpers/util');
var req_core = require('../helpers/service/reqcore');
var qrCodeController = require('./QrCodeController');

/**
 * Getting the product media list & detail
 * */

function getProductMediaList(req, res) {
  //Eventually pass data to the page render obj
  var pageRender = {};

  //Building page components, asking them to a config
  pageRender.menu = utility.getMenuForPage(1);
  pageRender.pageHeader = utility.getPageHeader(12);

  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {

    var idProduct = req.params.idProduct;

    req_core.getAllProductsExternal(token, idProduct,function (e,o) {
      if(!e && o) {
        pageRender.productMediaList = JSON.stringify(o);
        pageRender.productId = idProduct;
        pageRender.userMail = req.session.userMail;

        res.render('admin/product-media-list', pageRender);
      }
    });
  }
}

function getProductMediaDetail(req, res) {
  var token = req.session.token;
  if (token == null) {
    res.redirect('/');
  } else {

    //Eventually getting the id of the promo!
    var mediaId = req.params.mediaId;

    //Eventually pass data to the page render obj
    var pageRender = {};
    var mediaObj = {};
    mediaObj.qrCode = {};

    //Building page components, asking them to a config
    pageRender.menu = utility.getMenuForPage(5);

    //Sending mail to page (top right display)
    pageRender.userMail = req.session.userMail;

    if(mediaId){
      //Doing an async call
      pageRender.pageHeader = utility.getPageHeader(14);//Edit
      req_core.getMediaById(token, mediaId, function (e,o){
        if (!e && o) {
          //adding to session current filename if exist
          pageRender.mediaObj = o;
          pageRender.mediaObj.QRCode = qrCodeController.buildQrCodeObj(o.QRCode);
          pageRender.mediaObj.Id_prodotto = o.Id_prodotto;

          pageRender.media = JSON.stringify(pageRender.mediaObj);
          res.render('admin/product-media-detail', pageRender);
        }
      });
    }else {
      pageRender.pageHeader = utility.getPageHeader(13);//New
      pageRender.mediaObj = {};
      pageRender.mediaObj.QRCode = {};
      pageRender.mediaObj.Id_prodotto = req.query.productId;
      pageRender.media = JSON.stringify(pageRender.mediaObj);
      res.render('admin/product-media-detail', pageRender);
    }

  }
}

//Post a media
function postMedia(req, res) {
  var token = req.session.token;
  if(token == null) {
    res.redirect('/');
  }else {
    var obj = _buildMedia(req);
    //var objJson = JSON.stringify(obj); Commented, useless, stringify is down in req_core method!
    var qrCode = req.body.cbQrCode;
    var qrCodeTitle = req.body.TitoloQrCode;

    req_core.insertMedia(token, obj, function (e, o, statusCode) {
      if (!e && o && (statusCode == 200 || statusCode == 201)) {
        //Eventually clear some session variables eg: Image or attached file when inserted
        if(qrCode && qrCode === 'on'){
          var qrObj  = {};
          qrObj.Nome = qrCodeTitle ? qrCodeTitle : utility.generateRandomName();
          qrObj.Tipo_risorsa = 'Tab_Multimedia';
          qrObj.Id_risorsa = o[0].Id;
          req_core.assignToFreeQr(token,qrObj,function (e,o){
            if(!e && o ){
              res.status(statusCode).send('{"status":"ok"}');
            }else {
              res.status(statusCode).send(e);
            }
          })
        }else {
          //Insert w/o a QR
          res.status(statusCode).send('{"status":"ok"}');
        }
      } else {
        res.status(statusCode).send(e);
      }
    });
  }
}

function putMedia(req, res) {

  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {

    var id = req.params.mediaId;
    var media = _buildMedia(req);

    //CHECK IF QRCODE AND REMOVE OR SET
    var qrCode = req.body.cbQrCode;
    var qrCodeTitle = req.body.TitoloQrCode;
    var Id_qrcode = req.body.Id_qrcode;

    if(!qrCode && Id_qrcode) {
      //Removing an assigned qrcode from a resource
      media.Id_qrcode_old = parseInt(Id_qrcode);
      media.Id_qrcode = -1;
    }

    req_core.updateMedia(token, media, id, function (e,o) {
      if(!e && o) {
        if(qrCode && qrCode === 'on' && !Id_qrcode){
          var qrObj  = {};
          qrObj.Nome = qrCodeTitle ? qrCodeTitle : utility.generateRandomName();
          qrObj.Tipo_risorsa = 'Tab_Multimedia';
          qrObj.Id_risorsa = o.Id;
          req_core.assignToFreeQr(token,qrObj,function (e,o){
            if(!e && o ){
              res.status(200).send('{"status":"ok"}');
            }else {
              res.status(500).send(e);
            }
          })
        }else {
          res.status(200).send('{"status":"ok"}');
        }
      }else {
        res.status(500).send(e);
      }
    });
  }
}

function deleteMedia(req, res) {

  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {
    var id = req.params.mediaId;
    req_core.deleteMedia(token, id, function (e,o) {
      if(!e && o) {
        res.status(200).send('{"status":"ok"}');
      }else {
        res.status(500).send(e);
      }
    });
  }
}


function _buildMedia(req) {
  var obj = {};
  //Setting title
  obj.Titolo = req.body.Nome;

  obj.Contenuto = req.body.Contenuto;

  obj.Id_prodotto = parseInt(req.query.productId);

  return obj;
}

exports.getProductMediaList = getProductMediaList;
exports.getProductMediaDetail = getProductMediaDetail;
exports.postMedia = postMedia;
exports.putMedia = putMedia;
exports.deleteMedia = deleteMedia;