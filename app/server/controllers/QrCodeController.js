/**
 * Crafted with ♥ by Alex on 22/12/2016.
 */

"use strict";
var MODULE_NAME = 'QrCodeController';
var QRCODE_EP = 'https://lcmstorage.blob.core.windows.net/qrcodes/';
var LCM_HOST_QR = process.env.LCM_HOST_QR;
const _ = require('lodash');

//Added utility class helper
var utility = require('../helpers/util');
var req_core = require('../helpers/service/reqcore');

const qrcodeTypes = [
  {name:'Categoria Prodotto', value:'sottocat', mapping:'Tab_Shop_Sottocategorie', getFunction:req_core.getAllSottocategorie},
  {name:'Risorse Collegate - Contenuto Multimediale', value:'multimedia', mapping:'Tab_Multimedia', getFunction:req_core.getAllMultimedia},
  {name:'Risorse Collegate - Documento (Cataloghi, Schede Tecniche, Schede Funzionali)', value:'allegati', mapping:'Tab_Allegati', getFunction:req_core.getAllAllegati},
  {name:'Risorse Collegate - Galleria fotografica', value:'gallery', mapping:'Tab_Gallerie', getFunction:req_core.getAllGallerie},
  {name:'Promozione', value:'promo', mapping:'Tab_Promo', getFunction:req_core.getAllPromos},
  {name:'Risorsa Esterna (Url)', value:'url', mapping:'Tab_Url', getFunction:req_core.getAllUrl}
];

function getQrCodeDetail(req, res) {
  //Getting token access...
  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {
    //Eventually pass data to the page render obj
    var pageRender = {};

    //Building page components, asking them to a config
    pageRender.menu = utility.getMenuForPage(4);
    pageRender.pageHeader = utility.getPageHeader(4);

    pageRender.qrCodeTypes = qrcodeTypes;

    //Asking WS for news list, on this callback call promos list
    if(req.params.qrCodeId) {
      req_core.getQrCodeById(req.params.qrCodeId, token, function (e, o) {
        if (!e && o) {
          //Assign correct vars to pageRender obj
          var qrCodeObj = buildQrCodeObj(o);
          pageRender.qrCode = qrCodeObj;
          pageRender.qrCodeString = JSON.stringify(qrCodeObj);
          res.render('admin/qrcode-detail', pageRender);
        } else {
          res.status(e.statusCode).send();
        }
      });
    }
    else {
      pageRender.qrCode = {};
      pageRender.qrCodeString = JSON.stringify({});
      res.render('admin/qrcode-detail', pageRender);
    }
  }
}

function putQrCodeDetail(req, res) {
  //Getting token access...
  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {
    //Eventually pass data to the page render obj
    var pageRender = {};

    //Building page components, asking them to a config
    pageRender.menu = utility.getMenuForPage(4);
    pageRender.pageHeader = utility.getPageHeader(4);

    var qrcodeObj = _buildQrCode(req);

    req_core.updateQrCode(token, qrcodeObj, req.params.qrCodeId, function (e, o, statusCode) {
      if (!e && o && statusCode == 200) {
        res.status(statusCode).send(o);
      }
      else {
        res.status(statusCode).send(e);
      }
    });
  }
}

function postQrCode(req, res) {
  //Getting token access...
  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {
    //Eventually pass data to the page render obj
    var pageRender = {};

    //Building page components, asking them to a config
    pageRender.menu = utility.getMenuForPage(4);
    pageRender.pageHeader = utility.getPageHeader(4);

    var qrcodeObj = _buildQrCode(req);

    req_core.assignToFreeQr(token, qrcodeObj, function (e, o, statusCode){
      if (!e && o && (statusCode == 201 || statusCode == 200)) {
        res.status(statusCode).send(o);
      } else {
        res.status(statusCode).send(e);
      }
    });
  }
}

function getQrCodeList(req, res) {
  //Getting token access...
  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {
    //Eventually pass data to the page render obj
    var pageRender = {};

    //Building page components, asking them to a config
    pageRender.menu = utility.getMenuForPage(4);
    pageRender.pageHeader = utility.getPageHeader(4);

    var qrList = [];
    //Asking WS for news list, on this callback call promos list
    req_core.getAllQrCode(token, function (e, o) {
      if (!e && o) {
        //Setting correct fields wich will be readed bt the table
        qrList = _.map(o,function(item){
          return buildQrCodeObj(item);
        });

        //Assign correct vars to pageRender obj and call homepage
        pageRender.qrList = JSON.stringify(qrList);

        //Sending mail to page (top right display)
        pageRender.userMail = req.session.userMail;

        res.render('admin/qrcodes', pageRender);
      } else {
        res.status(404).send();
      }
    });
  }
}

function getQrCodeResources(req, res) {
  var token = req.session.token;

  if (token == null) {
    res.statusCode(401).send();
  } else {
    //Asking WS for resource list
    var type = req.query.type;
    var idResource = req.query.idResource;

    if (type && type !== '') {
      var typeObj = _.find(qrcodeTypes, _.matchesProperty('value', type));
      typeObj.getFunction(token, function (e, o, statusCode) {
        if (!e && o) {
          var data;
          //Removing items w qrcode
          if(!idResource) {
            data = _.filter(o, function (item) {return item.Id_qrcode == null;});
          }else {
            data  = _.filter(o, function (item) {return item.Id == idResource;});
            /*
            var filteredData = [];
            var mElem = {};
            for(var i=0; i<o.length; i++){
              if(idResource == o[i].Id){
                mElem = o[i];
              }else {
                filteredData.push(o[i]);
              }
            }
            filteredData.splice(0, 0, mElem);
            data = filteredData;
            */
          }
          var obj = {
            columns : buildDataTable(data, type),
            data : data
          };
          
          res.status(statusCode).send(obj);
        }
        else {
          res.status(statusCode).send(e);
        }
      });
    }
    else {
     res.status(400).send({message: 'Il campo \'type\' è necessario'});
    }
  }
}

function buildQrCodeObj(rawObj) {

  if (rawObj) {
    var objCode = getCorrectQrName(rawObj.Id,6);
    rawObj.Codice = objCode;
    rawObj.Icona = QRCODE_EP + "sd/" + objCode + ".png";
    rawObj.Tipologia = getCorrectQrType(rawObj.Tipo_risorsa);
    rawObj.HdVersion = QRCODE_EP + "hd/" + objCode + ".png";
    rawObj.FileName = objCode + '.png';
    rawObj.PublicUrl = LCM_HOST_QR + '/' + objCode;
    return rawObj;
  }
  else {
    return {};
  }
}

function getCorrectQrName (number, length) {
  var string = '' + number;
  while (string.length < length) {
    string = '0' + string;
  }

  return string;
}

function getCorrectQrType (type) {
  if(type === 'Tab_Url'){
    return 'Url';
  }else if(type === 'Tab_Promo'){
    return 'Promozione';
  }else if(type === 'Tab_Gallerie'){
    return 'Galleria fotografica';
  }else if(type === 'Tab_Multimedia'){
    return 'Contenuto multimediale';
  }else if(type === 'Tab_Allegati'){
    return 'Documento';
  }else if(type === 'Tab_Shop_Sottocategorie'){
    return 'Categoria Prodotto';
  }else {
    return 'ND';
  }
}

function _buildQrCode(req) {

  var qrcode = {};

  qrcode.Nome = req.body.Titolo;
  var typeObj = _.find(qrcodeTypes, _.matchesProperty('value', req.body.ddlTypes));
  qrcode.Tipo_risorsa = typeObj.mapping;
  qrcode.Id_risorsa = req.body.resourceId;

  return qrcode;
}


function buildDataTable(data, type) {

  var columns;
  //Building columns based on type
  if(type === 'sottocat'){

    _.each(data,function (item){
      item.MatchButton = "<a type='button' class='btn btn-default btn-md' data-item='"+item.Id+"' onClick='matchQrCode(this)' title='Abbina'>Abbina</a>"
    });

    columns = [
      {'title': 'Titolo', 'name': 'Titolo', 'data': 'Titolo', 'orderable': true},
      {'title': 'Descrizione', 'name': 'Descrizione', 'data': 'Description', 'orderable': false},
      {'title': 'Azione', 'name': '', 'data': 'MatchButton', 'orderable': false, 'width' : '90px'}
    ];
  }else if(type === 'multimedia'){

    _.each(data,function (item){
      item.MatchButton = "<a type='button' class='btn btn-default btn-md' data-item='"+item.Id+"' onClick='matchQrCode(this)' title='Abbina'>Abbina</a>"
    });

    columns = [
      {'title': 'Titolo', 'name': 'Titolo', 'data': 'Titolo', 'orderable': true},
      {'title': 'Contenuto', 'name': 'Contenuto', 'data': 'Contenuto', 'orderable': false},
      {'title': 'Azione', 'name': '', 'data': 'MatchButton', 'orderable': false, 'width' : '90px'}
    ];
  }else if(type === 'allegati'){

    _.each(data,function (item){
      item.MatchButton = "<a type='button' class='btn btn-default btn-md' data-item='"+item.Id+"' onClick='matchQrCode(this)' title='Abbina'>Abbina</a>"
    });

    columns = [
      {'title': 'Nome allegato', 'name': 'Nome allegato', 'data': 'Nome', 'orderable': true},
      {'title': 'Tipo allegato', 'name': 'Tipo allegato', 'data': 'Tipo_allegato', 'orderable': false},
      {'title': 'Azione', 'name': '', 'data': 'MatchButton', 'orderable': false, 'width' : '90px'}
    ];

  }else if(type === 'gallery'){

    _.each(data,function (item){
      item.MatchButton = "<a type='button' class='btn btn-default btn-md' data-item='"+item.Id+"' onClick='matchQrCode(this)' title='Abbina'>Abbina</a>"
      item.Attiva = item.Attiva ? "Sì" : "No";
    });

    columns = [
      {'title': 'Nome galleria', 'name': 'Nome galleria', 'data': 'Nome', 'orderable': true},
      {'title': 'Attiva', 'name': 'Attiva', 'data': 'Attiva', 'orderable': false},
      {'title': 'Azione', 'name': '', 'data': 'MatchButton', 'orderable': false, 'width' : '90px'}
    ];

  }else if(type === 'promo'){

    _.each(data,function (item){
      //Edit button
      item.MatchButton = "<a type='button' class='btn btn-default btn-md' data-item='"+item.Id+"' onClick='matchQrCode(this)' title='Abbina'>Abbina</a>"

      //Promo status
      try {
        var mStartDate = new Date(item.Data_inizio);
        var mEndDate = new Date(item.Data_fine);
        var mDate = new Date();
        if ((mDate <= mEndDate && mDate >= mStartDate)) {
          item.Status = 'Attiva';
          item.StatusCode = 1;
        }else {
          item.Status = 'Scaduta';
          item.StatusCode = 0;
        }
        //mPromos.push(mPromo);
      } catch (e) {}
    });

    //Removing expired promos
    _.filter(data, function(item) { return item.StatusCode == 1; });

    columns = [
      {'title': 'Titolo', 'name': 'Titolo', 'data': 'Titolo', 'orderable': true},
      {'title': 'Descrizione', 'name': 'Descrizione', 'data': 'Descrizione', 'orderable': false},
      {'title': 'Azione', 'name': '', 'data': 'MatchButton', 'orderable': false, 'width' : '90px'}
    ]
  }else if(type === 'url'){

    _.each(data,function (item){
      item.MatchButton = "<a type='button' class='btn btn-default btn-md' data-item='"+item.Id+"' onClick='matchQrCode(this,true)' title='Abbina'>Abbina</a>"
    });

    columns = [
      {'title': 'Nome', 'name': 'Nome', 'data': 'Nome', 'orderable': true},
      {'title': 'Contenuto', 'name': 'Contenuto', 'data': 'Contenuto', 'orderable': false},
      {'title': 'Azione', 'name': '', 'data': 'MatchButton', 'orderable': false, 'width' : '90px'}
    ];
  }
  return columns;
}


exports.getQrCodeList = getQrCodeList;
exports.getQrCodeDetail = getQrCodeDetail;
exports.getQrCodeResources = getQrCodeResources;
exports.putQrCodeDetail = putQrCodeDetail;
exports.postQrCode = postQrCode;
exports.buildQrCodeObj= buildQrCodeObj;
