/**
 * Crafted with ♥ by Alex on 28/11/2016.
 */

var req_core = require('../helpers/service/reqcore');

//Getting the total QR and response to the page
function getTotalQr (req, res) {

  //Doing the real request
  req_core.getTotalActiveQr(req.session.token, function (e,o) {
    var obj = {};
    obj.total = 'ND';

    if(!e && o) {
      obj.total = o;
    }

    res.status(200).send(obj);

  });

}


//Same job as first on diff collection
function getTotalActivePromo (req, res) {

  req_core.getAllPromos(req.session.token, function (e,o) {
    var obj = {};
    obj.total = 'ND';

    if(!e && o) {
      obj.total = getTotalActivePromosForNow(o);
    }

    res.status(200).send(obj);

  });

}


//Same job as first on diff collection
function getTotalNews (req, res) {

  req_core.getAllNews(req.session.token, function (e,o) {
    var obj = {};
    obj.total = 'ND';

    if(!e && o) {
      obj.total = o.length;
    }

    res.status(200).send(obj);

  });

}

//Same job as first on diff collection
function getTotalAttachedFiles (req, res) {

  req_core.getTotalAttachedFiles(req.session.token, function (e,o) {
    var obj = {};
    obj.total = 'ND';

    if(!e && o) {
      obj.total = o.length;
    }

    res.status(200).send(obj);

  });
}

//Checkin if a promo is active in this moment or it's expires
function getTotalActivePromosForNow (promos) {
  var total = 0;
  if(promos) {
    for (var i = 0; i < promos.length; i++) {
      var mPromo = promos[i];
      try {
        var mStartDate = new Date(mPromo.Data_inizio);
        var mEndDate = new Date(mPromo.Data_fine);
        var mDate = new Date();
        if ((mDate <= mEndDate && mDate >= mStartDate)) {
          total++;
        }
      } catch (e) {
      }
    }
  }
  return total;
}

exports.getTotalQr = getTotalQr;
exports.getTotalActivePromo = getTotalActivePromo;
exports.getTotalNews = getTotalNews;
exports.getTotalAttachedFiles = getTotalAttachedFiles;