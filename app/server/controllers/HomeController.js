/**
 * Crafted with ♥ by Alex on 21/11/2016.
 */
"use strict";
var MODULE_NAME = 'HomeController';

//Added utility class helper
var utility = require('../helpers/util');
var req_core = require('../helpers/service/reqcore');
var qrCodeController = require('./QrCodeController');
var _ = require('lodash');

/**
 * gets the home page of the CMS
 * @param req
 * @param res
 */
function getHome(req, res) {
  var token = req.session.token;
  if (token == null) {
    res.redirect('/');
  } else {
    //Eventually pass data to the page render obj
    var pageRender = {};

    //Building page components, asking them to a config
    pageRender.menu = utility.getMenuForPage(0);// Passing 0 because 0 is home page
    pageRender.pageHeader = utility.getPageHeader(0);

    //Sending mail to page (top right display)
    pageRender.userMail = req.session.userMail;

    //Adding pagePanels obj to page render, to show them in the current page
    pageRender.pagePanels = [
      {
        'tooltip': 'qr-code attivi sul network',
        'total': 0,
        'class': 'fa fa-qrcode',
        'title': 'Qr-Code',
        'uri': '/admin/stats/totalQr'
      },
      {
        'tooltip': 'totale promozioni attive',
        'total': 0,
        'class': 'fa fa-tags',
        'title': 'Promozioni',
        'uri': '/admin/stats/totalPromo'
      },
      {
        'tooltip': 'totale delle news per gli aderenti',
        'total': 0,
        'class': 'fa fa-align-left',
        'title': 'News',
        'uri': '/admin/stats/totalNews'
      },
      {
        'tooltip': 'totale degli allegati multimediali ai prodotti',
        'total': 0,
        'class': 'fa fa-play-circle-o',
        'title': 'Allegati multimediali',
        'uri': '/admin/stats/totalAttach'
      }
    ];

    //Todo: get need list of objs and send to datatable as String!
    var newsList = [];
    var qrList = [];

    //Asking WS for news list, on this callback call promos list
    req_core.getAllNews(token, function (e,o) {
      if (!e && o) {
        var newsList = o.slice(0, 5); //Taking first 5 elements
        _.map(newsList, function (item) {
          item.Url_immagine = item.Url_immagine ? item.Url_immagine : '/img/noimage.png';
          utility.setNewsFormattedDate(item);
        });
      }

      req_core.getAllQrCode(token, function (e, o) {
        if (!e && o) {
          //Setting each promo its own status 'Expired or Active'
          qrList = o.slice(0, 5);
          _.map(qrList, function (item) {
            qrCodeController.buildQrCodeObj(item);
          });
        }

        //Assign correct vars to pageRender obj and call homepage
        pageRender.newsList = JSON.stringify(newsList);
        pageRender.qrList = JSON.stringify(qrList);

        res.render('admin/home', pageRender);

      });
    });
  }
}

//exports
exports.getHome = getHome;