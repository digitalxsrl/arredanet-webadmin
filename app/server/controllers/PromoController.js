/**
 * Crafted with ♥ by Alex on 30/11/2016.
 */
"use strict";
var MODULE_NAME = 'PromoController';
var UPLOAD_FAIL = -1;
var MAX_TRY_NUM  = 5;
//Added utility class helper
var utility = require('../helpers/util');
var req_core = require('../helpers/service/reqcore');
var qrCodeController = require('./QrCodeController');
var fs = require('fs');
var _ = require('lodash');
const moment = require('moment-timezone');

var uploadController = require('./UploadController');

/**
 * gets the promolist page of the CMS
 * @param req
 * @param res
 */
function getPromoList(req, res) {
  //Getting token access...
  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {
    //Eventually pass data to the page render obj
    var pageRender = {};

    //Building page components, asking them to a config
    pageRender.menu = utility.getMenuForPage(2);// Passing 0 because 0 is home page
    pageRender.pageHeader = utility.getPageHeader(2);

    var promoList = [];
    //Asking WS for news list, on this callback call promos list
    req_core.getAllPromos(token, function (e, o) {
      if (!e && o) {

        req.session.uploadedImage = null;
        req.session.uploadedFile = null;

        _.map(o, function (item) {
          item.Url_immagine = item.Url_immagine ? item.Url_immagine : '/img/noimage.png';
          utility.setPromoStatus(item);
        });
      }

      //Assign correct vars to pageRender obj and call homepage
      pageRender.promoList = JSON.stringify(o);

      //Sending mail to page (top right display)
      pageRender.userMail = req.session.userMail;

      res.render('admin/promos', pageRender);

    });
  }
}

// Get promo detail page edit or new
function getPromoDetail(req, res) {

  var token = req.session.token;
  if (token == null) {
    res.redirect('/');
  } else {

    //Eventually getting the id of the promo!
    var idPromo = req.params.promoId;

    //Eventually pass data to the page render obj
    var pageRender = {};
    var promoObj = {};
    promoObj.QRCode = {};

    //Building page components, asking them to a config
    pageRender.menu = utility.getMenuForPage(5);

    //Sending mail to page (top right display)
    pageRender.userMail = req.session.userMail;

    if(idPromo){
      //Doing an async call
      pageRender.pageHeader = utility.getPageHeader(9);//Edit
      req_core.getPromoById(token, idPromo, function (e,o){
        if (!e && o) {
          //adding to session current filename if exist
          pageRender.promoObj = o;
          pageRender.promoObj.QRCode = qrCodeController.buildQrCodeObj(o.QRCode);

          //ContentHost Ep
          var contentHost = process.env['LCM_CONTENT_HOST_' + process.env.BUILD_TYPE];

          //Adding Preview for Image and PDF if Exist
          if(pageRender.promoObj.Nome_allegato) {
            req.session.uploadedFile = pageRender.promoObj.Nome_allegato;
            pageRender.promoObj.filePreview = contentHost + '/file/' + pageRender.promoObj.Nome_allegato;
            pageRender.promoObj.fileRemoveUrl = process.env.EP + "/admin/content/fileRemove"
          }else {
            req.session.uploadedFile = null;
          }

          if(pageRender.promoObj.Nome_immagine){
            req.session.uploadedImage = pageRender.promoObj.Nome_immagine;
            pageRender.promoObj.imagePreview = contentHost + '/image/' + pageRender.promoObj.Nome_immagine;
            pageRender.promoObj.imageRemoveUrl = process.env.EP + "/admin/content/imageRemove"
          }else {
            req.session.uploadedImage = null;
          }

          pageRender.promo = JSON.stringify(pageRender.promoObj);
          res.render('admin/promo-detail', pageRender);
        }
      });
    } else {
      pageRender.pageHeader = utility.getPageHeader(7);//New
      pageRender.promoObj = {};
      pageRender.promoObj.QRCode = {};
      pageRender.promo = JSON.stringify(pageRender.promoObj);
      res.render('admin/promo-detail', pageRender);
    }
  }
}

//Post a promo
function postPromo(req, res) {
  var token = req.session.token;
  if(token == null) {
    res.redirect('/');
  }else {
    var obj = _buildPromo(req);
    //var objJson = JSON.stringify(obj); Commented, useless, stringify is down in req_core method!
    var qrCode = req.body.cbQrCode;
    var qrCodeTitle = req.body.TitoloQrCode;

    req_core.insertPromo(token, obj, function (e, o, statusCode) {
      if (!e && o && (statusCode == 200 || statusCode == 201)) {
        //Eventually clear some session variables eg: Image or attached file when inserted

        req.session.uploadedImage = null;
        req.session.uploadedFile = null;

        if(qrCode && qrCode === 'on'){

          var qrObj  = {};
          qrObj.Nome = qrCodeTitle ? qrCodeTitle : utility.generateRandomName();
          qrObj.Tipo_risorsa = 'Tab_Promo';
          qrObj.Id_risorsa = o[0].Id;
          req_core.assignToFreeQr(token,qrObj,function (e,o){
            if(!e && o ){
              res.status(statusCode).send('{"status":"ok"}');
            }else {
              res.status(statusCode).send(e);
            }
          })
        }else {
          //Insert w/o a QR
          res.status(statusCode).send('{"status":"ok"}');
        }
      } else {
        res.status(statusCode).send(e);
      }
    });
  }
}

function putPromo(req, res) {
  var token = req.session.token;
  if(token == null) {
    res.redirect('/');
  }else {

    var obj = _buildPromo(req);
    var promoId = req.params.promoId;

    //CHECK IF QRCODE AND REMOVE OR SET
    var qrCode = req.body.cbQrCode;
    var qrCodeTitle = req.body.TitoloQrCode;
    var Id_qrcode = req.body.Id_qrcode;

    if(!qrCode && Id_qrcode) {
      //Removing an assigned qrcode from a resource
      obj.Id_qrcode_old = parseInt(Id_qrcode);
      obj.Id_qrcode = -1;
    }

    if (promoId) {
      req_core.updatePromo(token, obj, promoId, function (e, o, statusCode) {
        if (!e && o && statusCode == 200) {

          req.session.uploadedImage = null;
          req.session.uploadedFile = null;

          if (qrCode && qrCode === 'on' && !Id_qrcode) {
            var qrObj = {};
            qrObj.Nome = qrCodeTitle ? qrCodeTitle : utility.generateRandomName();
            qrObj.Tipo_risorsa = 'Tab_Promo';
            qrObj.Id_risorsa = o[0].Id;
            req_core.assignToFreeQr(token, qrObj, function (e, o) {
              if (!e && o) {
                res.status(200).send('{"status":"ok"}');
              } else {
                res.status(500).send(e);
              }
            })
          } else {
            res.status(200).send('{"status":"ok"}');
          }
        } else {
          res.status(statusCode).send(e);
        }
      });
    }
  }
}

function deletePromo(req, res) {
  var token = req.session.token;
  if(token == null) {
    res.redirect('/');
  }else {
    var promoId = req.params.promoId;
    if (promoId) {
      req_core.deletePromo(token, promoId, function (e, o) {
        if (!e && o) {
          res.status(200).send('{"status":"ok"}');
        } else {
          res.status(500).send('{"status":"error"}');
        }
      });
    }
  }
}

function _buildPromo(req) {
  var obj = {};
  //Setting title
  obj.Titolo = req.body.Titolo;

  //Setting description
  obj.Descrizione = req.body.Descrizione_hidden;

  //Setting dates
  obj.Data_inizio = moment(parseDate(req.body.init_date)).format("YYYY-MM-DD HH:mm:ss");
  obj.Data_fine = moment(parseDate(req.body.expire_date)).format("YYYY-MM-DD HH:mm:ss");


  obj.Nome_immagine =  req.session.uploadedImage ? req.session.uploadedImage : '';
  obj.Nome_allegato = req.session.uploadedFile ? req.session.uploadedFile : '';

  return obj;
}

function postNewsMainImage(req, res) {
  uploadController.postFile(req,res,uploadController.formData().resource.PROMO, uploadController.formData().type.image)
}

function postFile (req, res) {
  uploadController.postFile(req,res,uploadController.formData().resource.PROMO, uploadController.formData().type.file);
}


// parse a date in mm/dd/yyyy format HELPER
// 25/11/2015 11:06
function parseDate(input) {
  var parts = input.split(" ");
  var date = parts[0].split("/");
  var format = date[1]+"/"+date[0]+"/"+date[2]+ " " + parts[1];
  return new Date(format);
}


//exports
exports.getPromoList = getPromoList;
exports.getPromoDetail = getPromoDetail;
exports.postPromo = postPromo;
exports.putPromo = putPromo;
exports.deletePromo = deletePromo;
exports.postNewsMainImage = postNewsMainImage;
exports.postFile = postFile;
