/**
 * Crafted with ♥ by Alex on 23/12/2016.
 */

"use strict";
var MODULE_NAME = 'UrlController';
//Added utility class helper
var utility = require('../helpers/util');
var req_core = require('../helpers/service/reqcore');
var qrCodeController = require('./QrCodeController');
var RequestModule = require('request');
/**
 * gets the promolist page of the CMS
 * @param req
 * @param res
 */
function getUrlList(req, res) {
  //Getting token access...
  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {
    //Eventually pass data to the page render obj
    var pageRender = {};

    //Building page components, asking them to a config
    pageRender.menu = utility.getMenuForPage(6);
    pageRender.pageHeader = utility.getPageHeader(10);


    //Asking WS for news list, on this callback call promos list
    req_core.getAllUrl(token, function (e, o) {
      //Assign correct vars to pageRender obj and call homepage
      pageRender.urlList = JSON.stringify(o);

      //Sending mail to page (top right display)
      pageRender.userMail = req.session.userMail;

      res.render('admin/urls', pageRender);

    });
  }
}

// Get promo detail page edit or new
function getUrlDetail(req, res) {

  var token = req.session.token;
  if (token == null) {
    res.redirect('/');
  } else {

    //Eventually getting the id of the promo!
    var idUrl = req.params.idUrl;

    //Eventually pass data to the page render obj
    var pageRender = {};
    var mediaObj = {};
    mediaObj.qrCode = {};

    //Building page components, asking them to a config
    pageRender.menu = utility.getMenuForPage(5);

    //Sending mail to page (top right display)
    pageRender.userMail = req.session.userMail;

    if(idUrl){
      //Doing an async call
      pageRender.pageHeader = utility.getPageHeader(11);//Edit
      req_core.getUrlById(token, idUrl, function (e,o){
        if (!e && o) {
          //adding to session current filename if exist
          pageRender.urlObj = o;
          pageRender.urlObj.QRCode = qrCodeController.buildQrCodeObj(o.QRCode);

          pageRender.url = JSON.stringify(pageRender.urlObj);
          res.render('admin/url-detail', pageRender);
        }
      });
    }else {
      pageRender.pageHeader = utility.getPageHeader(10);//New
      pageRender.urlObj = {};
      pageRender.urlObj.QRCode = {};
      pageRender.url = JSON.stringify(pageRender.urlObj);
      res.render('admin/url-detail', pageRender);
    }

  }
}

//Post a url
function postUrl(req, res) {
  var token = req.session.token;
  if(token == null) {
    res.redirect('/');
  }else {
    var obj = _buildUrl(req);
    //var objJson = JSON.stringify(obj); Commented, useless, stringify is down in req_core method!
    var qrCode = req.body.cbQrCode;
    var qrCodeTitle = req.body.TitoloQrCode;

    req_core.insertUrl(token, obj, function (e, o, statusCode) {
      if (!e && o && (statusCode == 200 || statusCode == 201)) {
        //Eventually clear some session variables eg: Image or attached file when inserted
        if(qrCode && qrCode === 'on'){

          var qrObj  = {};
          qrObj.Nome = qrCodeTitle ? qrCodeTitle : utility.generateRandomName();
          qrObj.Tipo_risorsa = 'Tab_Url';
          qrObj.Id_risorsa = o[0].Id;
          req_core.assignToFreeQr(token,qrObj,function (e,o){
            if(!e && o ){
              res.status(statusCode).send('{"status":"ok"}');
            }else {
              res.status(statusCode).send(e);
            }
          })
        }else {
          //Insert w/o a QR
          res.status(statusCode).send('{"status":"ok"}');
        }
      } else {
        res.status(statusCode).send(e);
      }
    });
  }
}

function putUrl(req, res) {
  var token = req.session.token;
  if(token == null) {
    res.redirect('/');
  }else {

    var urlId = req.params.idUrl;
    var obj = _buildUrl(req);

    //CHECK IF QRCODE AND REMOVE OR SET
    var qrCode = req.body.cbQrCode;
    var qrCodeTitle = req.body.TitoloQrCode;
    var Id_qrcode = req.body.Id_qrcode;

    if(!qrCode && Id_qrcode) {
      //Removing an assigned qrcode from a resource
      obj.Id_qrcode_old = parseInt(Id_qrcode);
      obj.Id_qrcode = -1;
    }

    if (urlId) {
      req_core.updateUrl(token, obj, urlId, function (e, o, statusCode) {
        if (!e && o && statusCode == 200) {
          if(qrCode && qrCode === 'on' && !Id_qrcode){
            var qrObj  = {};
            qrObj.Nome = qrCodeTitle ? qrCodeTitle : utility.generateRandomName();
            qrObj.Tipo_risorsa = 'Tab_Url';
            qrObj.Id_risorsa = o[0].Id;
            req_core.assignToFreeQr(token,qrObj,function (e,o){
              if(!e && o ){
                res.status(200).send('{"status":"ok"}');
              }else {
                res.status(500).send(e);
              }
            })
          }else {
            res.status(200).send('{"status":"ok"}');
          }
        } else {
          res.status(statusCode).send(e);
        }
      });
    }
  }
}

function deleteUrl(req, res) {
  var token = req.session.token;
  if(token == null) {
    res.redirect('/');
  }else {
    var urlId = req.params.idUrl;
    if (urlId) {
      req_core.deleteUrl(token, urlId, function (e, o) {
        if (!e && o) {
          res.status(200).send('{"status":"ok"}');
        } else {
          res.status(500).send('{"status":"error"}');
        }
      });
    }
  }
}


//TODO
function _buildUrl(req) {
  var obj = {};
  //Setting title
  obj.Nome = req.body.Nome;

  obj.Contenuto = req.body.Contenuto;

  return obj;
}



//exports
exports.getUrlList = getUrlList;
exports.getUrlDetail = getUrlDetail;
exports.postUrl = postUrl;
exports.putUrl = putUrl;
exports.deleteUrl = deleteUrl;
