/**
 * Crafted with ♥ by Alex on 21/11/2016.
 */

"use strict";
var MODULE_NAME = 'LoginController';

var COOKIE_MAX_AGE = 900000;

var req_core = require('../helpers/service/reqcore');

/**
 * get for the root (/) routes --> manage login
 * @param req
 * @param res
 */
function rootGetForLogin(req, res) {

  // check if the user's credentials are saved in a cookie //
  if (req.cookies && (!req.cookies.user && req.cookies.user != "undefined") || (!req.cookies.pass && req.cookies.pass != "undefined")) {
    res.render('login', { title: 'Hello - Please Login To Your Account' });
  } else {
    // attempt automatic login //
    req_core.attemptLogin(
      false,
      req.cookies.user.toLowerCase(),
      req.cookies.pass,
      function (error, obj) {
        if (!obj) {
          res.status(400).send(error);
        } else {
          if (obj.error || obj.statusCode === 403 || obj.error === "Not Authorized") {
            res.redirect('/'); //redirect to login if not authorized or problems
          } else {
            req.session.token = obj.token;
            req.session.userMail = req.cookies.user.toLowerCase();
            //console.log(MODULE_NAME + ': successfully logged in [' + req.body.user + '] with pass[' + req.body.pass + ']');
            res.redirect('/admin/home');
          }
        }
      }
    );
  }
}

/**
 * post for the root (/) routes --> manage login
 * @param req
 * @param res
 */
function rootPostForLogin(req, res) {
  var username = req.body.user.toLowerCase();
  var password= req.body.pass;

  req_core.attemptLogin(
    true,
    username,
    password,
    function (error, obj) {
      if (!obj) {
        res.status(400).send(error);
      } else {
        if (obj.error || obj.error === "Not Authorized") {
          res.status(400).send(obj.error);
        } else {
          //Get other needed objects before home page, redirect to home otherwise
          req.session.token = obj.token;
          //var token = req.session.token;
          if (req.body['remember-me'] == 'true') {
            res.cookie('user', req.body.user.toLowerCase(), {maxAge: COOKIE_MAX_AGE});
            res.cookie('pass', req_core.hashPassword(req.body.pass), {maxAge: COOKIE_MAX_AGE});
          }
          req.session.userMail = req.body.user.toLowerCase();
          //console.log(MODULE_NAME + ': successfully logged in [' + req.body.user + '] with pass[' + req.body.pass + ']');
          res.status(200).send(obj);
        }
      }
    }
  );
}

/**
 * perform logout
 * @param req
 * @param res
 */
function logout (req, res) {
  // if user is requiring a logout
  res.clearCookie('user');
  res.clearCookie('pass');
  //Destroy session
  req.session = null;
  res.redirect('/');
  //req.session.destroy(
  //  function onDestroy(error) {
  //    res.redirect('/');
  //  }
  //);
}

//exports
exports.logout = logout;
exports.rootGetForLogin = rootGetForLogin;
exports.rootPostForLogin = rootPostForLogin;