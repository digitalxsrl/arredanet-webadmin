/**
 * Crafted with ♥ by Alex on 30/11/2016.
 */
"use strict";
var MODULE_NAME = 'NewsController';
var uploadController = require('./UploadController');
var fs = require('fs');
var _ = require('lodash');

var UPLOAD_FAIL = -1;
var MAX_TRY_NUM  = 5;

//Added utility class helper
var utility = require('../helpers/util');
var req_core = require('../helpers/service/reqcore');

/**
 * gets the home page of the CMS
 * @param req
 * @param res
 */
function getNewsList(req, res) {
  //Getting token access...
  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {
    //Eventually pass data to the page render obj
    var pageRender = {};

    //Clearing session images
    req.session.uploadedImage = null;
    req.session.uploadedFile = null;

    //Building page components, asking them to a config
    pageRender.menu = utility.getMenuForPage(3);// Passing 0 because 0 is home page
    pageRender.pageHeader = utility.getPageHeader(3);

    var newsList = [];
    //Asking WS for news list, on this callback call promos list
    req_core.getAllNews(token, function (e, o) {
      if (!e && o) {
        _.map(o, function (item) {
          item.Url_immagine = item.Url_immagine ? item.Url_immagine : '/img/noimage.png';
          utility.setNewsFormattedDate(item);
        });
      }

      //Assign correct vars to pageRender obj and call homepage
      pageRender.newsList = JSON.stringify(o);

      //Sending mail to page (top right display)
      pageRender.userMail = req.session.userMail;

      res.render('admin/newses', pageRender);

    });
  }
}

function getNewsDetail(req, res) {
  //Getting token access...
  var token = req.session.token;

  if (token == null) {
    res.redirect('/');
  } else {
    //Getting news id, if exists is a edit, news insert othwerwise
    var newsId = req.params.newsId;
    var newsObj = {};

    //Eventually pass data to the page render obj
    var pageRender = {};

    //Building page components, asking them to a config
    pageRender.menu = utility.getMenuForPage(5);//General internal page

    if(newsId) {
      req_core.getNewsById(token, newsId, function (e,o) {
        if(!e && o){
          //If there is no errors...
          pageRender.pageHeader = utility.getPageHeader(8);
          pageRender.newsObj = o[0];

          //ContentHost Ep
          var contentHost = process.env['LCM_CONTENT_HOST_' + process.env.BUILD_TYPE];

          //Adding Preview for Image and PDF if Exist
          if(pageRender.newsObj.Nome_allegato) {
            req.session.uploadedFile = pageRender.newsObj.Nome_allegato;
            pageRender.newsObj.filePreview = contentHost + '/file/' + pageRender.newsObj.Nome_allegato;
            pageRender.newsObj.fileRemoveUrl = process.env.EP + "/admin/content/fileRemove"
          }else {
            req.session.uploadedFile = null;
          }

          if(pageRender.newsObj.Nome_immagine){
            req.session.uploadedImage = pageRender.newsObj.Nome_immagine;
            pageRender.newsObj.imagePreview = contentHost + '/image/' + pageRender.newsObj.Nome_immagine;
            pageRender.newsObj.imageRemoveUrl = process.env.EP + "/admin/content/imageRemove"
          }else {
            req.session.uploadedImage = null;
          }

          pageRender.news = JSON.stringify(pageRender.newsObj);

          res.render('admin/news-detail', pageRender);
        }//What do else?
      });
    }else {
      pageRender.pageHeader = utility.getPageHeader(6);
      pageRender.news = JSON.stringify(newsObj);
      pageRender.newsObj = newsObj;

      res.render('admin/news-detail', pageRender);
    }
  }
}

//Post a news
function postNews(req, res) {
  var token = req.session.token;
  if(token == null) {
    res.redirect('/');
  }else {
    var obj = _buildNews(req);
    //var objJson = JSON.stringify(obj); Commented, useless, stringify is down in req_core method!

    req_core.insertNews(token, obj, function (e, o, statusCode) {
      if (!e && o && statusCode == 200) {
        //Eventually clear some session variables eg: Image or attached file when inserted
        req.session.uploadedImage = null;
        req.session.uploadedFile = null;
        res.status(statusCode).send('{"status":"ok"}');
      } else {
        res.status(statusCode).send(e);
      }
    });
  }
}

function putNews(req, res) {
  var token = req.session.token;
  if(token == null) {
    res.redirect('/');
  }else {
    var obj = _buildNews(req);
    //var objJson = JSON.stringify(obj); Commented, useless, stringify is down in req_core method!
    var newsId = req.params.newsId;
    if (newsId) {
      req_core.updateNews(token, obj, newsId, function (e, o, statusCode) {
        if (!e && o && statusCode == 200) {
          req.session.uploadedImage = null;
          req.session.uploadedFile = null;
          res.status(statusCode).send('{"status":"ok"}');
        } else {
          res.status(statusCode).send(e);
        }
      });
    }
  }
}

function deleteNews(req, res) {
  var token = req.session.token;
  if(token == null) {
    res.redirect('/');
  }else {
    var newsId = req.params.newsId;
    if (newsId) {
      req_core.deleteNews(token, newsId, function (e, o) {
        if (!e && o) {
          res.status(200).send('{"status":"ok"}');
        } else {
          res.status(500).send('{"status":"error"}');
        }
      });
    }
  }
}


//TODO
function _buildNews(req) {
  var obj = {};
  //Setting title
  obj.Titolo = req.body.Titolo;

  //Setting description
  obj.Descrizione = req.body.Descrizione_hidden;

  obj.Nome_immagine =  req.session.uploadedImage ? req.session.uploadedImage : '';
  obj.Nome_allegato = req.session.uploadedFile ? req.session.uploadedFile : '';

  return obj;
}

function postNewsMainImage(req, res) {
  uploadController.postFile(req,res,uploadController.formData().resource.NEWS, uploadController.formData().type.image);
}

function postFile (req, res) {
  uploadController.postFile(req,res,uploadController.formData().resource.NEWS, uploadController.formData().type.file);
}

//exports
exports.getNewsList = getNewsList;
exports.getNewsDetail = getNewsDetail;
exports.postNews = postNews;
exports.putNews = putNews;
exports.deleteNews = deleteNews;
exports.postNewsMainImage = postNewsMainImage;
exports.postFile = postFile;