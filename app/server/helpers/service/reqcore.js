/**
 * Crafted with ♥ by Alex on 21/11/2016.
 */

var reqService = require('../service/reqservice');
var crypto = require('crypto');

//CMS User login
exports.attemptLogin = function (manual, user, pass, callback) {
  //Generate hash
  var hash = crypto.createHash('sha256').update(pass).digest('base64');

  //Building user obj for the body
  var _user = {};
  _user.username = user;
  if(manual){
    _user.password = hash;
  }else {
    _user.password = pass;
  }

  //Build and execute the request
  var header = {"Content-Type": "application/json"};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];

  //Sending the _user obj to the service via a REST client
  reqService.makeRequest(ep, "POST", header, JSON.stringify(_user), "/login", callback);

};

//HASHING a given password
exports.hashPassword = function (psw) {
  return crypto.createHash('sha256').update(psw).digest('base64');
};

// Getting the total Qr code on the system
exports.getTotalActiveQr = function (token, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/qrcode/count", callback)
};

// Getting all products attached files
exports.getTotalAttachedFiles = function (token, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/allegati", callback);
};
/**
 * PROMO
 * */
// Getting all promos
exports.getAllPromos = function (token, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/promo", callback)
};

// Getting a promo by id
exports.getPromoById = function (token, promoId ,callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/promo/"+promoId, callback)
};

//Posting a new news
exports.insertPromo = function (token, promo,callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "POST", header, JSON.stringify(promo), "/promo", callback)
};

//Updating a news
exports.updatePromo = function (token, promo, promoId ,callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "PUT", header, JSON.stringify(promo), "/promo/"+promoId, callback)
};

//Deleting a promo
exports.deletePromo = function (token, promoId, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "DEL", header, null, "/promo/"+promoId, callback);
};



/**
 * NEWS
 * */
// Getting all newses
exports.getAllNews = function (token, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/news", callback)
};

// Getting news by id
exports.getNewsById = function (token, newsId, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/news/"+newsId, callback)
};

//Posting a new news
exports.insertNews = function (token, news,callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "POST", header, JSON.stringify(news), "/news", callback)
};

//Updating a news
exports.updateNews = function (token, news, newsId ,callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "PUT", header, JSON.stringify(news), "/news/"+newsId, callback)
};

//Deleting a news
exports.deleteNews = function (token, newsId, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "DEL", header, null, "/news/"+newsId, callback);
};

/**
 * QrCode part
 * */
// Getting all qrcodes
exports.getAllQrCode = function (token, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/qrcode", callback);
};

// Getting all qrcodes
exports.getQrCodeById = function (qrCodeId, token, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/qrcode/"+qrCodeId, callback);
};

//Insert/Assign a free QRCode
exports.assignToFreeQr = function (token, resource,callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "POST", header, JSON.stringify(resource), "/qrcode", callback)
};

//Updating a QRcode
exports.updateQrCode = function (token, qrcode, qrcodeId ,callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "PUT", header, JSON.stringify(qrcode), "/qrcode/"+qrcodeId, callback)
};


/**
 * SOTTOCATEGORIE
 * */
// Getting all sottocategorie
exports.getAllSottocategorie = function (token, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/sottocategorie", callback)
};

/**
 * GALLERIE
 * */
// Getting all sottocategorie
exports.getAllGallerie = function (token, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/gallery", callback)
};

/**
 * MULTIMEDIA
 * */
// Getting all sottocategorie
exports.getAllMultimedia = function (token, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/multimedia", callback)
};

/**
 * ALLEGATI
 * */
// Getting all sottocategorie
exports.getAllAllegati = function (token, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/allegati", callback)
};

/**
 * URL
 * */
// Getting all sottocategorie
exports.getAllUrl = function (token, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/url", callback)
};
// Getting url by id
exports.getUrlById = function (token, urlId, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/url/"+urlId, callback)
};

//Posting a new url
exports.insertUrl = function (token, url,callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "POST", header, JSON.stringify(url), "/url", callback)
};

//Updating a url
exports.updateUrl = function (token, url, urlId ,callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "PUT", header, JSON.stringify(url), "/url/"+urlId, callback)
};

//Deleting a news
exports.deleteUrl = function (token, urlId, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "DEL", header, null, "/url/"+urlId, callback);
};

/**
 * PRODUCTS
 * */

// Getting all products
exports.getAllProducts = function (token, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/prodotti", callback);
};


// Getting all products attached files
exports.getAllProductsAttached = function (token, idProduct, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/allegati?productId="+idProduct, callback);
};
// update attachment
exports.updateAttachment = function (token, attachment, id, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "PUT", header, JSON.stringify(attachment), '/allegati/' + id, callback)
};
// delete attachment
exports.deleteAttachment = function (token, id, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "DEL", header, null, '/allegati/' + id, callback);
};
// get attachment by id
exports.getAttachmentById = function (token, attachId, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/allegati/"+attachId, callback)
};

//Posting a new attach
exports.insertAttachment = function (token, attachment,callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "POST", header, JSON.stringify(attachment), "/allegati", callback)
};

exports.getAllProductsExternal = function (token, idProduct,callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/multimedia?productId="+idProduct, callback);
};

exports.getAllProductGallery = function (token, idProduct, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/gallery?productId="+idProduct, callback);
};

// Getting media by id
exports.getMediaById = function (token, mediaId, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/multimedia/"+mediaId, callback)
};

//Posting a new media
exports.insertMedia = function (token, media,callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "POST", header, JSON.stringify(media), "/multimedia", callback)
};

//Updating a media
exports.updateMedia = function (token, media, mediaId ,callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "PUT", header, JSON.stringify(media), "/multimedia/"+mediaId, callback)
};

//Deleting a media
exports.deleteMedia = function (token, mediaId, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "DEL", header, null, "/multimedia/"+mediaId, callback);
};

//Gallerie
//Getting gallery by id
exports.getGalleryById = function (token, galleryId, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "GET", header, null, "/gallery/"+galleryId, callback)
};

//Posting a new gallery
exports.insertGallery = function (token, gallery,callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "POST", header, JSON.stringify(gallery), "/gallery", callback)
};

//Updating a gallery
exports.updateGallery = function (token, gallery, galleryId ,callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "PUT", header, JSON.stringify(gallery), "/gallery/"+galleryId, callback)
};

//Deleting a gallery
exports.deleteGallery = function (token, galleryId, callback) {
  var header = {
    "Content-Type": "application/json",
    "token": token};
  var ep = process.env['LCM_SERVICE_' + process.env.BUILD_TYPE];
  reqService.makeRequest(ep, "DEL", header, null, "/gallery/"+galleryId, callback);
};
