/**
 * Crafted with ♥ by Alex on 21/11/2016.
 */

var req = require('request');

/**
 * headers must be a json string like: {"x-access-token" : token}
 **/

exports.makeRequest = function (ep, type, header, body, route, callback) {
  req[type.toLowerCase()]({
    headers: header,
    body: body,
    url: ep + route
  }, function (error, response) {
    if (!error && response && response.statusCode != 401 && response.body) {
      try {
        var obj = JSON.parse(response.body);
        return callback(null, obj, response.statusCode)
      } catch (e) {
        //ToDo more check!
        error = e;
      }
    }else if(response.statusCode == 204) {
      return callback(null, {}, 200);
    }
    error = response;
    callback(error, null, response.statusCode ? response.statusCode : 500);

  });

};