/**
 * Crafted with ♥ by Alex on 21/11/2016.
 */

var shortid = require('shortid');
var moment = require('moment-timezone');

/**
 * Start logging the req handling time
 * */
exports._startLoggingTime = function (loggingContent) {

  var timingKey = loggingContent + "|" + shortid.generate();
  console.time(timingKey);

  return timingKey;

};

/**
 * Stop logging the req handling time
 * */
exports._stopLoggingTime = function (timingKey) {

  console.timeEnd(timingKey);

};

/**
 * Generate random name
 * */

exports.generateRandomName = function () {
  return shortid.generate();
};

/**
 * Handling table hours format
 * */
//Show list of news/event timing
function listFormatDate  (value) {
    var mDate = moment(value);
    return mDate.tz("Europe/Rome").format('DD/MM/YYYY - HH:mm');
}

//Setting newses list date formatted
exports.setNewsFormattedDate = function (news) {

  //Setting also correct time for list
  news.Data_inserimento_lista = listFormatDate(news.Data_inserimento);

  return news;
};

//editing promos by ref adding status
exports.setPromoStatus = function (promo) {
  var mPromo = promo;

  //Setting also correct time for list
  mPromo.Data_fine_lista = listFormatDate(mPromo.Data_fine);

  var mStartDate = new Date(mPromo.Data_inizio);
  var mEndDate = new Date(mPromo.Data_fine);
  var mDate = new Date();
  if ((mDate <= mEndDate && mDate >= mStartDate)) {
    mPromo.Status = 'Attiva';
    mPromo.StatusCode = 1;
  }else {
    mPromo.Status = 'Scaduta';
    mPromo.StatusCode = 0;
  }

  return mPromo;
};

//admin/helper that help us build the menu OBJ, based on needs
exports.getMenuForPage = function (pageIndex) {
  //Switching pageIndex, to know wich page we're talkin about
  var builderMenu;
  switch(pageIndex) {
    case 0: //Home Page
      builderMenu = [
        {'title':'Home', 'link':'#'},
        {'title':'Risorse collegate', 'link':'/admin/products'},
        {'title':'Promozioni', 'link':'/admin/promos'},
        {'title':'News aderenti', 'link':'/admin/newses'},
        {'title':'Qr-code', 'link':'/admin/qrcodes'},
        {'title':'Url', 'link':'/admin/urls'}];
      break;
    case 1: //Product page
      builderMenu = [
        {'title':'Home', 'link':'/admin/home'},
        {'title':'Risorse collegate', 'link':'#'},
        {'title':'Promozioni', 'link':'/admin/promos'},
        {'title':'News aderenti', 'link':'/admin/newses'},
        {'title':'Qr-code', 'link':'/admin/qrcodes'},
        {'title':'Url', 'link':'/admin/urls'}];
    break;
    case 2: //Promo page
      builderMenu = [
        {'title':'Home', 'link':'/admin/home'},
        {'title':'Risorse collegate', 'link':'/admin/products'},
        {'title':'Promozioni', 'link':'#'},
        {'title':'News aderenti', 'link':'/admin/newses'},
        {'title':'Qr-code', 'link':'/admin/qrcodes'},
        {'title':'Url', 'link':'/admin/urls'}];
    break;
    case 3: //News page
      builderMenu = [
        {'title':'Home', 'link':'/admin/home'},
        {'title':'Risorse collegate', 'link':'/admin/products'},
        {'title':'Promozioni', 'link':'/admin/promos'},
        {'title':'News aderenti', 'link':'#'},
        {'title':'Qr-code', 'link':'/admin/qrcodes'},
        {'title':'Url', 'link':'/admin/urls'}];
    break;
    case 4: //QrCode page
      builderMenu = [
        {'title':'Home', 'link':'/admin/home'},
        {'title':'Risorse collegate', 'link':'/admin/products'},
        {'title':'Promozioni', 'link':'/admin/promos'},
        {'title':'News aderenti', 'link':'/admin/newses'},
        {'title':'Qr-code', 'link':'#'},
        {'title':'Url', 'link':'/admin/urls'}];
    break;
    case 5: // Page that does not comes from a menu item (internal pages)
      builderMenu = [
        {'title':'Home', 'link':'/admin/home'},
        {'title':'Risorse collegate', 'link':'/admin/products'},
        {'title':'Promozioni', 'link':'/admin/promos'},
        {'title':'News aderenti', 'link':'/admin/newses'},
        {'title':'Qr-code', 'link':'/admin/qrcodes'},
        {'title':'Url', 'link':'/admin/urls'}];
    break;
    case 6:
      builderMenu = [
        {'title':'Home', 'link':'/admin/home'},
        {'title':'Risorse collegate', 'link':'/admin/products'},
        {'title':'Promozioni', 'link':'/admin/promos'},
        {'title':'News aderenti', 'link':'/admin/newses'},
        {'title':'Qr-code', 'link':'/admin/qrcodes'},
        {'title':'Url', 'link':'#'}];
      break;
  }
  return builderMenu;
};

//Getting page header based on page index (of course this is decided before ;))
exports.getPageHeader = function (pageIndex) {
  //Switching pageIndex, to know wich page we're talkin about
  var pageHeader;
  switch(pageIndex) {
    case 0:
      pageHeader = {'title':'Dashboard','subtitle':'Utilizza il menù per navigare tra i contenuti.', 'icon':'pe-7s-home'};
      break;
    case 1:
      pageHeader = {'title':'Risorse collegate','subtitle':'Elenco dei prodotti La Casa Moderna.', 'icon':'pe-7s-shopbag'};
      break;
    case 2:
      pageHeader = {'title':'Promozioni','subtitle':'Elenco delle promozioni La Casa Moderna.', 'icon':'pe-7s-ticket'};
      break;
    case 3:
      pageHeader = {'title':'News aderenti','subtitle':'Elenco delle notizie e aggiornamenti visibili alla rete aderenti.', 'icon':'pe-7s-news-paper'};
      break;
    case 4:
      pageHeader = {'title':'Qr-code','subtitle':'Elenco dei qr-code attivi sul network La Casa Moderna.', 'icon':'pe-7s-keypad'};
      break;
    case 5:
      pageHeader = {'title':'Galleria Fotografica','subtitle':'Completa i campi per aggiungere i contenuti multimediali al prodotto.', 'icon':'pe-7s-photo'};
      break;
    case 6:
      pageHeader = {'title':'Nuova news','subtitle':'Compila i campi richiesti per inserire una nuova notizia per gli aderenti.', 'icon':'pe-7s-news-paper'};
      break;
    case 7:
      pageHeader = {'title':'Nuova promozione','subtitle':'Completa i campi richiesti per inserire una nuova promozione.', 'icon':'pe-7s-ticket'};
      break;
    case 8:
      pageHeader = {'title':'Modifica news','subtitle':'Completa i campi richiesti per modificare la news.', 'icon':'pe-7s-ticket'};
      break;
    case 9:
      pageHeader = {'title':'Modifica promozione','subtitle':'Completa i campi richiesti per modificare la promozione.', 'icon':'pe-7s-ticket'};
      break;
    case 10:
      pageHeader = {'title':'Nuovo url','subtitle':'Compila i campi richiesti per inserire una nuovo url', 'icon':'pe-7s-news-paper'};
      break;
    case 11:
      pageHeader = {'title':'Modifica url','subtitle':'Compila i campi richiesti per modificare una nuovo url', 'icon':'pe-7s-ticket'};
      break;
    case 12:
      pageHeader = {'title':'Contenuti multimediali','subtitle':'Elenco dei contenuti multimediali del prodotto', 'icon':'pe-7s-link'};
      break;
    case 13:
      pageHeader = {'title':'Contenuti multimediali','subtitle':'Inserisci un nuovo contenuto multimediale', 'icon':'pe-7s-link'};
      break;
    case 14:
      pageHeader = {'title':'Contenuti multimediali','subtitle':'Modifica il contenuto multimediale esistende', 'icon':'pe-7s-link'};
      break;
    case 15:
      pageHeader = {'title':'Allegati','subtitle':'Elenco degli allegati del prodotto', 'icon':'pe-7s-file'};
      break;
    case 16:
      pageHeader = {'title':'Allegato','subtitle':'Inserisci un nuovo allegato', 'icon':'pe-7s-file'};
      break;
    case 17:
      pageHeader = {'title':'Allegato','subtitle':'Modifica un allegato esistente', 'icon':'pe-7s-file'};
      break;
    case 18:
      pageHeader = {'title':'Gallerie','subtitle':'Elenco gallerie del prodotto', 'icon':'pe-7s-photo-gallery'};
      break;
    case 19:
      pageHeader = {'title':'Galleria','subtitle':'Inserisci una nuova galleria', 'icon':'pe-7s-photo-gallery'};
      break;
    case 20:
      pageHeader = {'title':'Galleria','subtitle':'Modifica la galleria', 'icon':'pe-7s-photo-gallery'};
      break;
  }
  return pageHeader;
};